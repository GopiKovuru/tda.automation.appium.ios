

1.Run the test from terminal/command prompt

2.On the terminal, navigate to the folder

/tda.automation.appium.ios

3.Arguments to be passed are -Ddevicetype , -Dbrand , -Denv , -Dlocale
(optional) , -Dcucumber
.options (for tags)

4.-Ddevicetype : phone , tablet

5.-Dbrand can be : tuiuk , fc , detui , meinetui , betui , besj , nl , sv , fi ,
 nb , da

6.-Denv can be : test , uat , preprod , prod

7.-Dlocale can be : fr , nl (for BE apps) and de-DE (for DE apps)

8.-Dcountry can be : de , at , ch (for DE apps only)

8.-Dcucumber.options take the tag which we want to run

Eg : -Dcucumber.options="--tags @ioscompleteregression"

9.type in the below command for tuiuk

mvn test -Ddevicetype="phone" -Dbrand="tuiuk" -Denv="test" -Dcucumber.options="--tags @ioscompleteregression"

10.type in the below command for fc

mvn test -Ddevicetype="phone" -Dbrand="fc" -Denv="test" -Dcucumber.options="--tags @ioscompleteregression"


11.type in the below command for detui

mvn test -Ddevicetype="phone" -Dbrand="detui" -Denv="test" -Dcountry="de" -Dcucumber.options="--tags @ioscompleteregression"

12.type in the below command for meinetui

mvn test -Ddevicetype="phone" -Dbrand="meinetui" -Denv="test"
-Dcountry="at"
-Dcucumber.options="--tags @ioscompleteregression"

13.type in the below command for betui

mvn test -Ddevicetype="phone" -Dbrand="betui" -Denv="test"  -Dlocale="fr-BE"
-Dcucumber.options="--tags @ioscompleteregression"

14.type in the below command for besj

 mvn test -Ddevicetype="phone" -Dbrand="besj" -Denv="test"  -Dlocale="nl-BE" -Dcucumber.options="--tags @ioscompleteregression"

15.type in the below command for nl

mvn test -Ddevicetype="phone" -Dbrand="nl" -Denv="test"  -Dcucumber.options="--tags @ioscompleteregression"

16.type in the below command for sv

mvn test -Ddevicetype="phone" -Dbrand="sv" -Denv="test" -Dcucumber.options="--tags @ioscompleteregression"

17.type in the below command for fi

mvn test -Ddevicetype="phone" -Dbrand="fi" -Denv="test" -Dcucumber.options="--tags @ioscompleteregression"

18.type in the below command for nb

mvn test -Ddevicetype="phone" -Dbrand="nb" -Denv="test" -Dcucumber.options="--tags @ioscompleteregression"

19.type in the below command for da

mvn test -Ddevicetype="phone" -Dbrand="da" -Denv="test"-Dcucumber.options="--tags @ioscompleteregression"
