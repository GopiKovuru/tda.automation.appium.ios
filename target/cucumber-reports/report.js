$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Highlights.feature");
formatter.feature({
  "line": 1,
  "name": "Validates the Highlights Screen",
  "description": "",
  "id": "validates-the-highlights-screen",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Validates the Navigation Menu Items",
  "description": "",
  "id": "validates-the-highlights-screen;validates-the-navigation-menu-items",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@ioshighlights"
    },
    {
      "line": 3,
      "name": "@ioscompleteregression"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "TUI App is Launched and I allow notifications",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "I click on each Highlights Menu Item",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "I validate Highlights Screen",
  "keyword": "Then "
});
formatter.match({
  "location": "BaseSteps.iLaunchIOSApp()"
});
formatter.result({
  "duration": 1059792848,
  "status": "passed"
});
formatter.match({
  "location": "HighlightsSteps.iClickOnEachHighlightsMenuItem()"
});
formatter.result({
  "duration": 1325286213,
  "status": "passed"
});
formatter.match({
  "location": "HighlightsSteps.iValidateHighlightsScreen()"
});
formatter.result({
  "duration": 365683050,
  "status": "passed"
});
});