#!/usr/bin/env bash


export LC_CTYPE=en_US.UTF-8



if [ "$#" -ne "6" ]; then
echo "\nFive Arguments Needed"
echo "1)eg: tablet/phone"
echo "2)eg: brand : th or fc"
echo "3)eg: feature tags to be executed : @androidphonesanity"
echo "4)eg: Medium to be executed : sim/emu/deviceid"
echo "5)eg: environment to be executed : uktest/ukdev/ukpreprod"
echo "6)eg: Test Data size: 2"
echo "\nFOR SIMULATOR"
echo "\nSample command: \n 1) sh run_android_uk.sh phone th @androidphonesanityuk sim uktest 1..2"
echo "\nSample command: \n 2) sh run_android_uk.sh tablet fc @androidtabletsanituk sim uktest 1..2"
echo "\nSample command: \n 1) sh run_android_uk.sh phone th @androidphonesanityuk emu ukdev 1..2"
echo "\nSample command: \n 2) sh run_android_uk.sh tablet fc @androidtabletsanityuk emu ukdev 1..2"
echo "\nFOR DEVICE"
echo "\nSample command: \n 1) sh run_android_uk.sh phone th @androidphonesanityuk "AndroidDeviceID" uktest 1..2"
echo "\nSample command: \n 2) sh run_android_uk.sh tablet fc @androidtabletsanityuk "Android DeviceID" uktest 1..2"
echo "\nTo get AndroidDeviceID,use the command: adb devices"
else

    LANG=$2
    HW=$1
    TAGNAMES=$3
    DEVICE_ID=$4
    ENVIRONMENT=$5
    TESTDATASIZE=$6
    if [ $2 == "fc" ]  ; then
      USERS_DATA_FILE=features/support/fcusers.rb
      TEST_PACKAGE_NAME="com.firstchoice.myfirstchoice.dev"
      DEV_PACKAGE_NAME="com.firstchoice.myfirstchoice.dev"
      PREPROD_PACKAGE_NAME="com.firstchoice.myfirstchoice"
      PROD_PACKAGE_NAME="com.firstchoice.myfirstchoice"
      FILENAME="../../Appfiles/app-firstchoice-release-9.6.53.apk"
     elif [ $2 == "th"  ] ; then
      USERS_DATA_FILE=features/support/thusers.rb
      TEST_PACKAGE_NAME="com.thomson.mythomson.dev"
      DEV_PACKAGE_NAME="com.thomson.mythomson.dev"
      PREPROD_PACKAGE_NAME="com.thomson.mythomson"
      PROD_PACKAGE_NAME="com.thomson.mythomson"
      FILENAME="../../Appfiles/app-mythomson-release-9.6.53.apk"
    fi

    IFS=".." read -ra testData <<< "$TESTDATASIZE"

    FROMTESTDATE=${testData[0]}
    TOTESTDATA=${testData[2]}
    export FROMTESTDATE=$FROMTESTDATE
    export TOTESTDATA=$TOTESTDATA
    for (( i=FROMTESTDATE; i <= TOTESTDATA; ++i ))
    do

        DATE=`date +%d_%m_%Y_%H_%M_%S`

        LINE_COUNT=0
        B_FOUND=false
        DATA=""
        while read LINE ; do
          if [[ $LINE_COUNT -lt 3 ]] && [[ $B_FOUND == true ]] ; then
            DATA="${LINE}#${DATA}"
            LINE_COUNT=$LINE_COUNT+1
          fi
          if [[ $LINE = ":valid_garda_booking${i} => {" ]] ; then
            B_FOUND=true
          fi
        done < $USERS_DATA_FILE
        echo "Selected Booking Data: "$DATA

        IFS='#' read -r surname departuredate bookingref <<< "$DATA"
        SURNAME=`echo $surname | sed 's/:surname => //g' | sed 's/,//g' | sed 's/\"//g'`
        DEPARTUREDATE=`echo $departuredate | sed 's/:departuredate => //g' | sed 's/,//g'| sed 's/\"//g'`

        BOOKING_REF=`echo $bookingref | sed 's/:booking_reference => //g' | sed 's/,//g' | sed 's/\"//g'`

        echo "Hardware,tags and device selected:"
        echo $HW : $TAGNAMES : $DEVICE_ID
        echo $TAGNAMES
        export HW=$HW
        export TAGNAMES=$TAGNAMES
        export DEVICE_ID=$DEVICE_ID
        export FILENAME=$FILENAME
        export LANG=$LANG
        export ENVIRONMENT=$ENVIRONMENT
        export CURRENTTESTDATA=$i
        ANDROID_HOME=$HOME/Library/Android/sdk/
        ADB=adb
        #ADB=$ANDROID_HOME/platform-tools/adb
        which $ADB
            killall -9 adb
            sleep 2
            if [ $DEVICE_ID == "sim" ]  ; then
              echo "Launching Genymotion"
              if [ $HW == "phone" ] ; then
                sh start_device.sh "SamsungGalaxyS8" "genymotion"
              elif [ $HW == "tablet" ] ; then
                sh start_device.sh " SamsungGalaxyS6" "genymotion"
              fi
              sleep 5
                DEVICE_ID=`adb get-serialno`
                sleep 5
                adb shell input keyevent 82
                echo $DEVICE_ID
            elif [ $DEVICE_ID == "emu" ]  ; then
              echo "Launching Andoird Emulator"
              if [ $HW == "phone" ] ; then
                sh start_device.sh "S3" "emulator"
              elif [ $HW == "tablet" ] ; then
                sh start_device.sh  "GooglePixelC710" "emulator"
              fi
              sleep 5
                DEVICE_ID=`adb get-serialno`
                sleep 5
                adb shell input keyevent 82
                echo $DEVICE_ID
            fi
            $ADB devices

            if [ $DEVICE_ID  ] ; then
              ADB_DEVICE="-s "$DEVICE_ID
            else
              echo "No Device or Simulator specified"
              exit 1
            fi

            if [ $HW == "tablet" ] || [ $HW == "phone" ] ; then

                CUCUMBER_PROFILE=uk_android

                rm -rf test_servers/
                echo "Resigning APP"
                calabash-android resign $FILENAME
                calabash-android build $FILENAME

                TESTAPPINSTALLED=`$ADB $ADB_DEVICE shell pm list packages | grep $TEST_PACKAGE_NAME`
                DEVAPPINSTALLED=`$ADB $ADB_DEVICE shell pm list packages | grep $DEV_PACKAGE_NAME`
                PRODAPPINSTALLED=`$ADB $ADB_DEVICE shell pm list packages | grep $PROD_PACKAGE_NAME`
                PREPRODAPPINSTALLED=`$ADB $ADB_DEVICE shell pm list packages | grep $PREPROD_PACKAGE_NAME`
                if ! [ -z $TESTAPPINSTALLED ] ; then
                  echo "Uninstalling previous version of test app..."
                  $ADB $ADB_DEVICE shell am force-stop $TEST_PACKAGE_NAME
                  sleep 1
                  $ADB $ADB_DEVICE uninstall "$TEST_PACKAGE_NAME"
                  echo "Successfully uninstalled previous version of test build."
                fi
                if ! [ -z $DEVAPPINSTALLED ] ; then
                  echo "Uninstalling previous version of dev app..."
                  $ADB $ADB_DEVICE shell am force-stop $DEV_PACKAGE_NAME
                  sleep 1
                  $ADB $ADB_DEVICE uninstall "$DEV_PACKAGE_NAME"
                  echo "Successfully uninstalled previous version of dev build."
                fi
                if ! [ -z $PRODAPPINSTALLED ] ; then
                  echo "Uninstalling previous version of prod app..."
                  $ADB $ADB_DEVICE shell am force-stop $PROD_PACKAGE_NAME
                  sleep 1
                  $ADB $ADB_DEVICE uninstall "$PROD_PACKAGE_NAME"
                  echo "Successfully uninstalled previous version of prod build."
                fi
                if ! [ -z $PREPRODAPPINSTALLED ] ; then
                  echo "Uninstalling previous version of prod app..."
                  $ADB $ADB_DEVICE shell am force-stop $PREPROD_PACKAGE_NAME
                  sleep 1
                  $ADB $ADB_DEVICE uninstall "$PREPROD_PACKAGE_NAME"
                  echo "Successfully uninstalled previous version of pre prod build."
                fi

                rm -rf test_servers/


                echo "Resigning APP"
                calabash-android resign $FILENAME
                calabash-android build $FILENAME

                echo adb $ADB_DEVICE install -r $FILENAME
                adb $ADB_DEVICE install -r $FILENAME

                echo adb $ADB_DEVICE install -r test_servers/*.apk
                adb $ADB_DEVICE install -r test_servers/*.apk


                DEVICE_NAME=`adb $ADB_DEVICE shell getprop ro.product.model`
                DEVICE_MFR=`adb $ADB_DEVICE shell getprop ro.product.manufacturer`

                export DEVICE_NAME=$DEVICE_NAME
                export DEVICE_MFR=$DEVICE_MFR

                d_name="${DEVICE_NAME%?}"
                d_name=${d_name// /}
                d_name=$(echo "${DEVICE_NAME}" | tr -d '[[:space:]]')
                echo "Device Name: "$d_name
                REPORTS_DIR="reports/${LANG}-${d_name}/${DATE}-${BOOKING_REF}"
                echo "Reports dir: "$REPORTS_DIR
                mkdir -p $REPORTS_DIR
                html_report_name=$REPORTS_DIR/android-${d_name}-${SURNAME}-${BOOKING_REF}.html
                echo ADB_DEVICE_ARG=$DEVICE_ID SCREENSHOT_PATH=$REPORTS_DIR/ HW=$HW OS=android  bundle exec calabash-android run $FILENAME -p $CUCUMBER_PROFILE --tag $TAGNAMES -f html -o $html_report_name  -f json -o $REPORTS_DIR/j_report.json -f junit -o $REPORTS_DIR/junit | tee gopi.txt
                RESET_BETWEEN_SCENARIOS=1 DEBUG=0 ADB_DEVICE_ARG=$DEVICE_ID SCREENSHOT_PATH=$REPORTS_DIR/ HW=$HW OS=android bundle exec calabash-android run $FILENAME -p $CUCUMBER_PROFILE --tag $TAGNAMES -f html -o $html_report_name  -f json -o $REPORTS_DIR/j_report.json -f junit -o $REPORTS_DIR/junit

                ruby lib/html_image_path_updater.rb $html_report_name


             else
                echo "Please select the valid hardware : tablet/phone"
            fi
    done

fi