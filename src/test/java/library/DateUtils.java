//package library;
//
//import android.annotation.SuppressLint;
//import android.support.annotation.IntDef;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.annotation.StringDef;
//
//import com.tui.tda.data.utils.ArrayUtils;
//
//import org.joda.time.DateTime;
//import org.joda.time.Days;
//import org.joda.time.Weeks;
//import org.joda.time.Years;
//
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.Locale;
//import java.util.TimeZone;
//
//public class DateUtils {
//
//    public static final String FORMAT_TUI_DATE_PICKER = "d MMMM yyyy";
//    public static final String FORMAT_TUI_DATE_PICKER_WITH_DASHES = "dd-MMMM-yyyy";
//    public static final String FORMAT_TUI_DATE_PICKER_NO_YEAR = "d MMMM";
//    public static final String FORMAT_SIMPLE_DATE = "dd/MM/yyyy";
//    public static final String FORMAT_SIMPLE_DATE_DASH = "dd-MM-yyyy";
//    public static final String FORMAT_MONTH = "MMMM";
//    public static final String FORMAT_DAY_OF_WEEK = "EEEE";
//    public static final String FORMAT_SIMPLE_TIME = "HH:mm";
//    public static final String FORMAT_ZONE_TIMESTAMP = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
//    public static final String FORMAT_WITHOUT_ZONE_TIMESTAMP = "yyyy-MM-dd'T'HH:mm:ss";
//    public static final String FORMAT_DATE = "yyyy-MM-dd";
//    public static final String FORMAT_API_ZONE_TIMESTAMP = "EEE, dd MMM yyyy HH:mm:ss Z";
//    public static final String FORMAT_MESSAGES_TIMESTAMP = "MMMM dd, yyyy hh:mm:ss a";
//    public static final String FORMAT_RESULTS_DATE = "d MMM yyyy";
//    public static final String FORMAT_ZONE_HOMECARDS_TIMESTAMP = "yyyy-MM-dd'T'HH:mm:ssZ";
//    public static final String FORMAT_ABBREVIATED_MONTH = "d MMM";
//
//
//    public static Date getPastDate(Date date, int days) {
//        return getFutureDate(date, days * -1);
//    }
//
//    public static Date getFutureDate(Date date, int days) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        calendar.add(Calendar.DAY_OF_MONTH, days);
//        return calendar.getTime();
//    }
//
//    public static long getElapsedTime(Date timeBefore, Date timeAfter) {
//        long result = timeAfter.getTime() - timeBefore.getTime();
//        return (result < 0) ? 0 : result;
//
//    }
//
//    public static String getFormattedTime(Date date, String timeZoneName) {
//        if (date == null) {
//            throw new IllegalArgumentException("date must not be null");
//        }
//        java.text.DateFormat df = new SimpleDateFormat(FORMAT_SIMPLE_TIME, Locale.getDefault());
//        if (!StringUtils.isEmpty(timeZoneName)) {
//            df.setTimeZone(TimeZone.getTimeZone(timeZoneName));
//        }
//        return df.format(date);
//    }
//
//    public static String getFormattedDate(DateTime date, String timeZoneName) {
//        if (date == null) {
//            throw new IllegalArgumentException("date must not be null");
//        }
//        SimpleDateFormat df = getDateFormat();
//        if (!StringUtils.isEmpty(timeZoneName)) {
//            df.setTimeZone(TimeZone.getTimeZone(timeZoneName));
//        }
//        return date.toString(df.toPattern());
//    }
//
//    public static class MonthFormatterType {
//
//        public static final String MONTH_M = "M";
//
//        public static final String MONTH_MM = "MM";
//        public static final String MONTH_MMM = "MMM";
//        public static final String MONTH_MMMM = "MMMM";
//
//        @StringDef({MONTH_M, MONTH_MM, MONTH_MMM, MONTH_MMMM})
//        @Retention(RetentionPolicy.SOURCE)
//        public @interface Type {
//
//        }
//    }
//
//    public static class Months {
//
//        @IntDef({Calendar.JANUARY, Calendar.FEBRUARY, Calendar.MARCH, Calendar.APRIL,
//                Calendar.MAY, Calendar.JUNE, Calendar.JULY, Calendar.AUGUST, Calendar.SEPTEMBER,
//                Calendar.OCTOBER, Calendar.NOVEMBER, Calendar.DECEMBER})
//        @Retention(RetentionPolicy.SOURCE)
//        public @interface Type {
//
//        }
//    }
//
//    public static final int HOURS_IN_DAY = 24;
//
//    public static final int MINUTES_IN_HOUR = 60;
//    public static final int SECONDS_IN_MINUTE = 60;
//    public static final int MILLISECONDS_IN_SECOND = 1000;
//    public static final int DAY_TO_MILLISECONDS = HOURS_IN_DAY * MINUTES_IN_HOUR * SECONDS_IN_MINUTE * MILLISECONDS_IN_SECOND;
//
//    public static final int HOUR_TO_MILLISECONDS = MINUTES_IN_HOUR * SECONDS_IN_MINUTE * MILLISECONDS_IN_SECOND;
//    public static final int MINUTE_TO_MILLISECONDS = SECONDS_IN_MINUTE * MILLISECONDS_IN_SECOND;
//
//    private DateUtils() {
//    }
//
//    public static String getDatePattern(Locale locale) {
//        SimpleDateFormat sdf = (SimpleDateFormat) SimpleDateFormat.getDateInstance(java.text.DateFormat.SHORT, locale);
//        return revisePattern(sdf, locale);
//    }
//
//    private static String getShortDateTimePattern(Locale locale) {
//        SimpleDateFormat sdf = (SimpleDateFormat) SimpleDateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT, locale);
//        return revisePattern(sdf, locale);
//    }
//
//    private static String revisePattern(SimpleDateFormat simpleDateFormat, Locale locale) {
//        //We have old requirement from NL to replace - with / I requested Carla to validate if this is still necessary
//        //Till then we have to have this if/else as Sweden raised bug as they date format was affected by this too
//        String pattern = simpleDateFormat.toPattern().replaceAll("\\byy\\b", "yyyy");
//        return locale.getCountry().equalsIgnoreCase("NL") ? pattern.replace("-", "/") : pattern;
//    }
//
//    private static SimpleDateFormat getDateFormat() {
//        return new SimpleDateFormat(getDatePattern(Locale.getDefault()), Locale.getDefault());
//    }
//
//    private static SimpleDateFormat getShortDateTimeFormat() {
//        return new SimpleDateFormat(getShortDateTimePattern(Locale.getDefault()), Locale.getDefault());
//    }
//
//    private static SimpleDateFormat getDateTimeFormat() {
//        return new SimpleDateFormat(FORMAT_SIMPLE_TIME + " - " + getDatePattern(Locale.getDefault()), Locale.getDefault());
//    }
//
//    public static String formatSimpleDate(Calendar calendar) {
//        return formatSimpleDate(calendar.getTime());
//    }
//
//    public static String formatSimpleDate(Date calendar) {
//        if (calendar == null) {
//            throw new IllegalArgumentException("calendar must not be null");
//        }
//
//        return getDateFormat().format(calendar);
//    }
//
//    public static String formatSimpleDate(DateTime calendar) {
//        if (calendar == null) {
//            throw new IllegalArgumentException("calendar must not be null");
//        }
//
//        return calendar.toString(getDateFormat().toPattern());
//    }
//
//    public static String formatShortDateTime(Date calendar) {
//        if (calendar == null) {
//            throw new IllegalArgumentException("calendar must not be null");
//        }
//
//        return getShortDateTimeFormat().format(calendar);
//    }
//
//    public static String formatTuiDatePicker(Calendar calendar) {
//        return formatTuiDatePicker(calendar.getTime());
//    }
//
//    public static String formatTuiDatePicker(Date calendar) {
//        return formatTuiDatePicker(calendar, FORMAT_TUI_DATE_PICKER);
//    }
//
//    public static String formatTuiDatePicker(Date calendar, String format) {
//        if (calendar == null) {
//            throw new IllegalArgumentException("calendar must not be null");
//        }
//
//        return new SimpleDateFormat(format, Locale.getDefault()).format(calendar);
//    }
//
//    public static String formatToMonth(Date calendar) {
//        if (calendar == null) {
//            throw new IllegalArgumentException("calendar must not be null");
//        }
//
//        return new SimpleDateFormat(FORMAT_MONTH, Locale.getDefault()).format(calendar);
//    }
//
//    @SuppressLint("SimpleDateFormat")
//    public static String formatFlightDate(DateTime calendar) {
//        if (calendar == null) {
//            throw new IllegalArgumentException("calendar must not be null");
//        }
//
//        return calendar.toString(getDateTimeFormat().toPattern());
//    }
//
//    public static String formatFlightTime(Date date) {
//        return new SimpleDateFormat(FORMAT_SIMPLE_TIME, Locale.getDefault()).format(date);
//    }
//
//    public static String formatSimpleTime(DateTime date) {
//        return date.toString(FORMAT_SIMPLE_TIME);
//    }
//
//    public static String formatOpeningTime(Date date) {
//        return new SimpleDateFormat(FORMAT_SIMPLE_TIME, Locale.getDefault()).format(date);
//    }
//
//    public static String formatZoneTimestamp(Date date) {
//        return new SimpleDateFormat(FORMAT_ZONE_TIMESTAMP, Locale.getDefault()).format(date);
//    }
//
//    public static String formatAuthenticationDate(Date date) {
//        return new SimpleDateFormat(FORMAT_DATE, Locale.getDefault()).format(date);
//    }
//
//    public static String formatTimestampForMessages(Date date) {
//        return new SimpleDateFormat(FORMAT_MESSAGES_TIMESTAMP, Locale.getDefault()).format(date);
//    }
//
//    @Deprecated
//    public static boolean isInPast(String dateToParse, String format) {
//        final Date date = parseDate(dateToParse, format);
//        return isInPast(date);
//    }
//
//    public static boolean isInPastWithTime(String dateToParse, String format) {
//        final Date date = parseDate(dateToParse, format);
//        return isInPastWithTime(date);
//    }
//
//    @Deprecated
//    /**
//     * Widely used in app but..
//     * Need investigation why would compare dates without time.
//     */
//    public static boolean isInPast(Date date) {
//        return isInPast(date, true);
//    }
//
//    public static boolean isInPastWithTime(Date date) {
//        return isInPast(date, false);
//    }
//
//    private static boolean isInPast(Date date, boolean clearTime) {
//        if (date == null) {
//            throw new IllegalArgumentException("date must not be null");
//        }
//
//        Calendar currentDate = Calendar.getInstance();
//        if (clearTime) {
//            removeTime(currentDate);
//        }
//
//        Calendar compareDate = Calendar.getInstance();
//        compareDate.setTime(date);
//        if (clearTime) {
//            removeTime(compareDate);
//        }
//
//        return compareDate.before(currentDate);
//    }
//
//    public static Date getToday() {
//        return new Date();
//    }
//
//    public static boolean isToday(Date date) {
//        if (date == null) {
//            throw new IllegalArgumentException("date must not be null");
//        }
//
//        Calendar currentDate = Calendar.getInstance();
//        removeTime(currentDate);
//
//        Calendar compareDate = Calendar.getInstance();
//        compareDate.setTime(date);
//        removeTime(compareDate);
//
//        return compareDate.get(Calendar.YEAR) == currentDate.get(Calendar.YEAR)
//                && compareDate.get(Calendar.DAY_OF_YEAR) == currentDate.get(Calendar.DAY_OF_YEAR);
//    }
//
//    public static boolean isToday(Date dateNow, Date dateThen) {
//        if (dateNow == null || dateThen == null) {
//            throw new IllegalArgumentException("date must not be null");
//        }
//
//        Calendar currentDate = Calendar.getInstance();
//        currentDate.setTime(dateNow);
//        removeTime(currentDate);
//
//        Calendar compareDate = Calendar.getInstance();
//        compareDate.setTime(dateThen);
//        removeTime(compareDate);
//
//        return compareDate.get(Calendar.YEAR) == currentDate.get(Calendar.YEAR)
//                && compareDate.get(Calendar.DAY_OF_YEAR) == currentDate.get(Calendar.DAY_OF_YEAR);
//    }
//
//    public static boolean isYesterday(Date dateThen, Date dateNow) {
//        if (dateThen == null || dateNow == null) {
//            throw new IllegalArgumentException("date must not be null");
//        }
//
//        Calendar yesterday = Calendar.getInstance();
//        yesterday.setTime(dateNow);
//        yesterday.add(Calendar.DATE, -1);
//
//        Calendar timeThen = Calendar.getInstance();
//        timeThen.setTime(dateThen);
//
//        return yesterday.get(Calendar.DAY_OF_YEAR) == timeThen.get(Calendar.DAY_OF_YEAR);
//    }
//
//    @SuppressWarnings("WrongConstant")
//    @Months.Type
//    public static int getMonth(Date date) {
//        if (date == null) {
//            throw new IllegalArgumentException("date must not be null");
//        }
//
//        return date.getMonth();
//    }
//
//    @Deprecated
//    /**
//     * Widely used in app
//     * Need to investigate why would we compare dates without time.
//     */
//    public static boolean isInFuture(Date date) {
//        return isInFuture(date, true);
//    }
//
//    public static boolean isInFutureWithTime(Date date) {
//        return isInFuture(date, false);
//    }
//
//    private static boolean isInFuture(Date date, boolean clearTime) {
//        if (date == null) {
//            throw new IllegalArgumentException("date must not be null");
//        }
//
//        Calendar currentDate = Calendar.getInstance();
//        if (clearTime) {
//            removeTime(currentDate);
//        }
//
//        Calendar compareDate = Calendar.getInstance();
//        compareDate.setTime(date);
//        if (clearTime) {
//            removeTime(currentDate);
//        }
//
//        return compareDate.after(currentDate);
//    }
//
//    private static Calendar removeTime(Calendar calendar) {
//        calendar.clear(Calendar.HOUR);
//        calendar.clear(Calendar.MINUTE);
//        calendar.clear(Calendar.SECOND);
//        calendar.clear(Calendar.MILLISECOND);
//
//        return calendar;
//    }
//
//    public static int parseMonth(String month, @MonthFormatterType.Type String monthFormat) {
//        if (StringUtils.isEmpty(month)) {
//            throw new IllegalArgumentException("dateToParse must not be null or empty");
//        }
//        SimpleDateFormat format;
//        Date date = null;
//
//        try {
//            format = new SimpleDateFormat(monthFormat, Locale.getDefault());
//            date = format.parse(month);
//        } catch (IllegalArgumentException | ParseException e) {
//            e.printStackTrace();
//        }
//
//        return date.getMonth();
//    }
//
//    public static Date parseDate(String dateToParse, String dateFormat) {
//        if (StringUtils.isEmpty(dateToParse)) {
//            throw new IllegalArgumentException("dateToParse must not be null or empty");
//        }
//
//        if (StringUtils.isEmpty(dateFormat)) {
//            throw new IllegalArgumentException("dateFormat must not be null or empty");
//        }
//        dateToParse = dateToParse.replaceAll("\\s+", " ");
//        Date date = null;
//
//        try {
//            date = getDate(dateToParse, dateFormat);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return date;
//    }
//
//    @Nullable
//    public static Date getDate(String dateToParse, String dateFormat) throws IllegalArgumentException, ParseException {
//        if (dateToParse != null) {
//            SimpleDateFormat format;
//            format = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
//            return format.parse(dateToParse);
//        } else {
//            return null;
//        }
//    }
//
//    /**
//     * @param d1 first date to compare
//     * @param d2 second date to compare
//     * @return returns number of full days between the dates
//     * Deprecated due to moving to use JodaTime.
//     * Use daysBetweenWithJodaTime() if possible.
//     */
//    @Deprecated
//    public static int daysBetween(Date d1, Date d2) {
//        return (int) ((d2.getTime() - d1.getTime()) / (DAY_TO_MILLISECONDS));
//    }
//
//    public static int daysBetweenWithJodaTime(Date d1, Date d2) {
//        DateTime localStartDate = new DateTime(d1.getTime());
//        DateTime localEndDate = new DateTime(d2.getTime());
//        return Days.daysBetween(localStartDate.toLocalDate(), localEndDate.toLocalDate()).getDays();
//    }
//
//    /**
//     * @param d1           first date to compare
//     * @param d2           second date to compare
//     * @param numberOfDays number of days to remove
//     * @return returns number of full hours between the dates minus numberOfDays provided
//     */
//    public static int hoursBetween(Date d1, Date d2, long numberOfDays) {
//        return (int) ((d2.getTime() - d1.getTime() - (numberOfDays * DAY_TO_MILLISECONDS)) / HOUR_TO_MILLISECONDS);
//    }
//
//    public static int minutesBetween(Date d1, Date d2, long numberOfHours) {
//        return (int) ((d2.getTime() - d1.getTime() - (numberOfHours * HOUR_TO_MILLISECONDS)) / MINUTE_TO_MILLISECONDS);
//    }
//
//    public static int secondsBetween(Date d1, Date d2, long numberOfMinutes) {
//        return (int) ((d2.getTime() - d1.getTime() - (numberOfMinutes * MINUTE_TO_MILLISECONDS)) / MILLISECONDS_IN_SECOND);
//    }
//
//    /**
//     * @return Hours difference between dates (0 >= difference < 24)
//     */
//    public static int hoursDifference(Date d1, Date d2) {
//        return (int) ((d2.getTime() - d1.getTime()) % DAY_TO_MILLISECONDS) / HOUR_TO_MILLISECONDS;
//    }
//
//    /**
//     * @return Minutes difference between dates (0 >= difference < 60)
//     */
//    public static int minutesDifference(Date d1, Date d2) {
//        return (int) ((d2.getTime() - d1.getTime()) % HOUR_TO_MILLISECONDS) / MINUTE_TO_MILLISECONDS;
//    }
//
//    /**
//     * @return Seconds difference between dates (0 >= difference < 60)
//     */
//    public static int secondsDifference(Date d1, Date d2) {
//        return (int) ((d2.getTime() - d1.getTime()) % MINUTE_TO_MILLISECONDS) / MILLISECONDS_IN_SECOND;
//    }
//
//    public static String formatDate(Date date, String formatTimestamp) {
//        if (date == null) {
//            throw new IllegalArgumentException("calendar must not be null");
//        }
//
//        return new SimpleDateFormat(formatTimestamp, Locale.getDefault()).format(date);
//    }
//
//    /**
//     * returns local specific month name
//     *
//     * @param monthNumber where 0 is January and 11 is December
//     * @return returns localized name of month
//     */
//    public static String formatFullMonthName(Calendar cal, int monthNumber) {
//        if (monthNumber > 11 || monthNumber < 0) {
//            throw new IllegalArgumentException("month must be >= 0 and <=11");
//        }
//
//        cal.set(Calendar.DAY_OF_MONTH, 1);
//        cal.set(Calendar.MONTH, monthNumber);
//        return new SimpleDateFormat(FORMAT_MONTH, Locale.getDefault()).format(cal.getTime());
//    }
//
//    public static long unixDateToJavaDate(long unixTime) {
//        if (unixTime <= 0) {
//            throw new IllegalArgumentException("time should be bigger then 0");
//        }
//        return unixTime * MILLISECONDS_IN_SECOND;
//    }
//
//    public static Date longToLocalJavaDate(long timestamp) {
//        return new Date(timestamp + TimeZone.getDefault().getRawOffset());
//    }
//
//    public static Date longToUTCJavaDate(long timestamp) {
//        return new Date(timestamp);
//    }
//
//    public static String formatFullMonthName(int monthNumber) {
//        return formatFullMonthName(Calendar.getInstance(), monthNumber);
//    }
//
//    public static String formatDayOfWeek(Date date) {
//        if (date == null) {
//            throw new IllegalArgumentException("calendar must not be null");
//        }
//
//        return new SimpleDateFormat(FORMAT_DAY_OF_WEEK, Locale.getDefault()).format(date);
//    }
//
//    public static boolean isDateInPeriod(Date startDate, Date endDate, Date verifyDate) {
//        return startDate.getTime() <= verifyDate.getTime() && verifyDate.getTime() <= endDate.getTime();
//    }
//
//    public static boolean isSameDay(Date today, Date toCompare) {
//        SimpleDateFormat dateFormat = new SimpleDateFormat(FORMAT_SIMPLE_DATE, Locale.getDefault());
//        return dateFormat.format(today).equals(dateFormat.format(toCompare));
//    }
//
//    @SuppressLint("SimpleDateFormat")
//    @Nullable
//    public static String getStringFromDate(Date date) {
//        return date != null ? new SimpleDateFormat(FORMAT_SIMPLE_DATE).format(date) : null;
//    }
//
//    @NonNull
//    public static String getFormattedDate(@NonNull Date date, @NonNull String format) {
//        return new SimpleDateFormat(format, Locale.getDefault()).format(date);
//    }
//
//    @SuppressLint("SimpleDateFormat")
//    @Nullable
//    public static String getStringFromDateWithDashFormat(Date date) {
//        return date != null ? new SimpleDateFormat(FORMAT_DATE).format(date) : null;
//    }
//
//    @SuppressLint("SimpleDateFormat")
//    public static Date getDateFromStringApi(String stringDateValue) {
//        try {
//            return stringDateValue != null ? new SimpleDateFormat(FORMAT_DATE).parse(stringDateValue) : null;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public static List<Date> convertStringsToDates(List<String> values) {
//        List<Date> toRet = new ArrayList<>();
//        if (ArrayUtils.isNotEmpty(values)) {
//            for (String value : values) {
//                toRet.add(getDateFromStringApi(value));
//            }
//        }
//        return toRet;
//    }
//
//    public static int getCurrentTimeStamp() {
//        Long time = System.nanoTime();
//        return time.intValue();
//    }
//
//    private enum DifferenceType {
//        DAYS, WEEKS, YEARS
//    }
//
//    public static int getDaysBetweenDates(Date first, Date second) {
//        return getTimeBetweenDates(first, second, DifferenceType.DAYS);
//    }
//
//    public static int getWeeksBetweenDates(Date first, Date second) {
//        return getTimeBetweenDates(first, second, DifferenceType.WEEKS);
//    }
//
//    public static int getYearsBetweenDates(Date first, Date second) {
//        return getTimeBetweenDates(first, second, DifferenceType.YEARS);
//    }
//
//    private static int getTimeBetweenDates(Date first, Date second, DifferenceType type) {
//        DateTime dateTimeOne = new DateTime(first);
//        DateTime dateTimeSecond = new DateTime(second);
//        switch (type) {
//            case DAYS:
//                return Days.daysBetween(dateTimeOne, dateTimeSecond).getDays();
//            case WEEKS:
//                return Weeks.weeksBetween(dateTimeOne, dateTimeSecond).getWeeks();
//            default:
//                return Years.yearsBetween(dateTimeOne, dateTimeSecond).getYears();
//        }
//    }
//}