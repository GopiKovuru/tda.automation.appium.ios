package library;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

import java.lang.*;

import java.util.HashMap;

public class ReusableMethods {

    public HashMap<String, String> get_environments_data(String environmentName)
    {
        HashMap<String, String> environmentsHashMap = new HashMap<String,
                String>();
        try {
            File file = new File("src/test/resources/environments.xml");

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);

            document.getDocumentElement().normalize();

            NodeList nodeList = document.getElementsByTagName("environment");


            for (int index = 0; index < nodeList.getLength(); index++) {
                Node node = nodeList.item(index);


                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;

                    if(environmentName.equalsIgnoreCase("nonfcapplanga") || environmentName.equalsIgnoreCase("fcapplanga") ) {
                        if (environmentName.equalsIgnoreCase(eElement.getAttribute("env"))) {

                           environmentsHashMap.put("server_end_point", eElement.getElementsByTagName("server_end_point").item(0).getTextContent());
                           environmentsHashMap.put("authorization", eElement.getElementsByTagName("authorization").item(0).getTextContent());
                        }
                    }
                    else if (environmentName.equalsIgnoreCase(eElement.getAttribute("env"))) {



                            environmentsHashMap.put("x-api-key", eElement.getElementsByTagName("x-api-key").item(0).getTextContent());
                            environmentsHashMap.put("server_end_point", eElement.getElementsByTagName("server_end_point").item(0).getTextContent());
                            environmentsHashMap.put("x-access-token", eElement.getElementsByTagName("x-access-token").item(0).getTextContent());
                            environmentsHashMap.put("x-dynatrace", eElement.getElementsByTagName("x-dynatrace").item(0).getTextContent());
                            environmentsHashMap.put("correlation-id", eElement.getElementsByTagName("x-dynatrace").item(0).getTextContent());


                    }
                    else if (environmentName.equalsIgnoreCase(eElement.getAttribute("env"))) {



                        environmentsHashMap.put("x-api-key", eElement.getElementsByTagName("x-api-key").item(0).getTextContent());
                        environmentsHashMap.put("server_end_point", eElement.getElementsByTagName("server_end_point").item(0).getTextContent());
                        environmentsHashMap.put("x-access-token", eElement.getElementsByTagName("x-access-token").item(0).getTextContent());
                        environmentsHashMap.put("x-dynatrace", eElement.getElementsByTagName("x-dynatrace").item(0).getTextContent());
                        environmentsHashMap.put("correlation-id", eElement.getElementsByTagName("x-dynatrace").item(0).getTextContent());


                    }

                }
            }

        }
        catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        return environmentsHashMap;
    }

    public HashMap<String, String> get_testdata(String brand,String
            serialNo)
    {
        HashMap<String, String> testDataHashMap = new HashMap<String,
                String>();
        try {

            System.out.println("Brand is: " + brand);
            System.out.println("serialNo is: " + serialNo);
            File file = null ;

            String tagName = "";
            if (brand.equalsIgnoreCase("meinetui") || brand
                    .equalsIgnoreCase("detui"))
            {
                file = new File("src/test/resources/deusers.xml");
                tagName = "deusers";

            }
            else
            {
                file = new File("src/test/resources/" + brand + "users.xml");
                tagName = brand;
            }

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);

            document.getDocumentElement().normalize();

            NodeList nodeList = document.getElementsByTagName(tagName);


            for (int index = 0; index < nodeList.getLength(); index++) {
                Node node = nodeList.item(index);


                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;


                    if (serialNo.equalsIgnoreCase(eElement.getAttribute("sno"))) {


                        switch (brand) {
                            case "tuiuk":
                            case "fc":
                                testDataHashMap.put("surname", eElement.getElementsByTagName("surname").item(0).getTextContent());
                                testDataHashMap.put("booking_reference", eElement.getElementsByTagName("booking_reference").item(0).getTextContent());
                                testDataHashMap.put("departuredate", eElement.getElementsByTagName("departuredate").item(0).getTextContent());
                            case "detui":
                            case "meinetui":
                                testDataHashMap.put("username", eElement.getElementsByTagName("username").item(0).getTextContent());
                                testDataHashMap.put("password", eElement.getElementsByTagName("password").item(0).getTextContent());
                                testDataHashMap.put("booking_reference", eElement.getElementsByTagName("booking_reference").item(0).getTextContent());
                            case "betui":
                            case "besj":
                                testDataHashMap.put("surname", eElement.getElementsByTagName("surname").item(0).getTextContent());
                                testDataHashMap.put("booking_reference", eElement.getElementsByTagName("booking_reference").item(0).getTextContent());
                            case "nl":
                                testDataHashMap.put("surname", eElement.getElementsByTagName("surname").item(0).getTextContent());
                                testDataHashMap.put("booking_reference", eElement.getElementsByTagName("booking_reference").item(0).getTextContent());
                                testDataHashMap.put("gardaid", eElement.getElementsByTagName("gardaid").item(0).getTextContent());
                            case "sv":
                            case "fi":
                            case "nb":
                            case "da":
                                testDataHashMap.put("booking_reference", eElement.getElementsByTagName("booking_reference").item(0).getTextContent());
                                testDataHashMap.put("phone", eElement.getElementsByTagName("phone").item(0).getTextContent());
                                testDataHashMap.put("email", eElement.getElementsByTagName("email").item(0).getTextContent());
                                break;

                        }
                    }
                }
            }

        }
        catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        return testDataHashMap;
    }

}
