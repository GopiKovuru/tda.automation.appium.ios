package library;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;


import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;


public class GetLegoAPIData {

    private String brand;
    private String environment;
    private String filePath;
    public GetLegoAPIData(String environment,String brand) {

        this.brand = brand;
        this.environment = environment;
    }

    public String get_lego_api_data(String apiEndpoint) {
        String apiData = "";

        filePath = "src/test/java/com/tui/data/jsondata" +
                "/" + brand + "/";

        switch (brand) {
            case "tuiuk":
                apiData = get_lego_uk_api_data(apiEndpoint);
                break;
            case "nl":
                apiData =get_lego_nl_api_data(apiEndpoint) ;
                break;

        }
        return apiData;
    }

    private String get_lego_uk_api_data(String apiEndpoint) {

        String apiData = "";

        OkHttpClient client = new OkHttpClient();
        String stringEndpoint = "";
        ReusableMethods reusableMethods = new ReusableMethods();
        HashMap<String, String> environmentsHashMap = new HashMap<String,
                String>();

        switch (environment) {
            case "test":
                 environmentsHashMap = reusableMethods.get_environments_data("uktest");
                break;
            case "dev":
                environmentsHashMap = reusableMethods.get_environments_data
                        ("ukdev");
                break;
            case "uat":
                environmentsHashMap = reusableMethods.get_environments_data
                        ("ukuat");
                break;
            case "preprod":
                environmentsHashMap = reusableMethods.get_environments_data
                        ("ukpreprod");
                break;
            case "prod":
                environmentsHashMap = reusableMethods.get_environments_data
                        ("ukprod");
                break;
        }
        switch (apiEndpoint) {
            case "homecardswithoutbooking":
                stringEndpoint = environmentsHashMap.get("server_end_point") + "/api/v5/homecards";
                break;

        }
        try {

            Request request = new Request.Builder()
                    .url(stringEndpoint)
                    .get()
                    .addHeader("User-Agent", "UK-TH/9.8.0.4343 CFNetwork/901.1 Darwin/17.6.0")
                    .addHeader("correlation-id", environmentsHashMap.get
                            ("correlation-id"))
                    .addHeader("tui-app-img-bucket-size", "x-large")
                    .addHeader("app-locale", "en-GB")
                    .addHeader("x-access-token",  environmentsHashMap.get
                            ("x-access-token"))
                    .addHeader("device-id", "2AC15498-E322-4E5C-826B-F7A1B55B37D9")
                    .addHeader("device-category", "phone")
                    .addHeader("Accept-Language", "en-gb")
                    .addHeader("app-name", "tui-uk-th")
                    .addHeader("app-version", "9.8.0")
                    .addHeader("x-api-key", environmentsHashMap.get
                            ("x-api-key"))
                    .addHeader("x-dynatrace", environmentsHashMap.get
                            ("x-dynatrace"))
                    .addHeader("device-os", "iOS")
                    .build();

            Response response = client.newCall(request).execute();



            apiData = response.body().string();

            filePath = filePath + apiEndpoint + ".json";

            ObjectMapper mapper = new ObjectMapper();

            Object jsonObject = mapper.readValue(apiData, Object.class);
            String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
            try (FileWriter file = new FileWriter(filePath)) {

                file.write(prettyJson);
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("\nThis is exception");
            System.out.println(e.toString());
            return e.toString();
        }

        return apiData;
    }

    private String get_lego_nl_api_data(String apiEndpoint) {

        String apiData = "";

        OkHttpClient client = new OkHttpClient();
        String stringEndpoint = "";
        ReusableMethods reusableMethods = new ReusableMethods();
        HashMap<String, String> environmentsHashMap = new HashMap<String,
                String>();

        switch (environment) {
            case "test":
                environmentsHashMap = reusableMethods.get_environments_data
                        ("nltest");
                break;
            case "dev":
                environmentsHashMap = reusableMethods.get_environments_data
                        ("nldev");
                break;
            case "uat":
                environmentsHashMap = reusableMethods.get_environments_data
                        ("nluat");
                break;
            case "preprod":
                environmentsHashMap = reusableMethods.get_environments_data
                        ("nlpreprod");
                break;
            case "prod":
                environmentsHashMap = reusableMethods.get_environments_data
                        ("nlprod");
                break;
        }
        switch (apiEndpoint) {
            case "homecardswithoutbooking":
                stringEndpoint = environmentsHashMap.get("server_end_point") + "/api/v5/homecards";
                break;

        }
        try {

            Request request = new Request.Builder()
                    .url(stringEndpoint)
                    .get()
                    .addHeader("User-Agent", "UK-TH/9.8.0.4343 CFNetwork/901.1 Darwin/17.6.0")
                    .addHeader("correlation-id", environmentsHashMap.get
                            ("correlation-id"))
                    .addHeader("tui-app-img-bucket-size", "x-large")
                    .addHeader("app-locale", "nl_NL")
                    .addHeader("x-access-token",  environmentsHashMap.get
                            ("x-access-token"))
                    .addHeader("device-id", "2AC15498-E322-4E5C-826B-F7A1B55B37D9")
                    .addHeader("device-category", "phone")
                    .addHeader("Accept-Language", "en-gb")
                    .addHeader("app-name", "tui-nl")
                    .addHeader("app-version", "9.8.0")
                    .addHeader("x-api-key", environmentsHashMap.get
                            ("x-api-key"))
                    .addHeader("x-dynatrace", environmentsHashMap.get
                            ("x-dynatrace"))
                    .addHeader("device-os", "iOS")
                    .build();

            Response response = client.newCall(request).execute();
            apiData = response.body().string();

            ObjectMapper mapper = new ObjectMapper();

            Object jsonObject = mapper.readValue(apiData, Object.class);
            String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);

            filePath = filePath + apiEndpoint + ".json";


            try (FileWriter file = new FileWriter(filePath)) {

                file.write(prettyJson);
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("\nThis is exception");
            System.out.println(e.toString());
            return e.toString();
        }

        return apiData;
    }
}
