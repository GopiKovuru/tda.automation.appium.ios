package library;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;


import java.util.Date;
import java.util.List;


public class ReusableUIMethods {

    public void pick_ios_date(IOSDriver driver, String dateSelected) {

        WebElement DatePickerWheel1 =
                driver.findElement(By.xpath("//XCUIElementTypeDatePicker"));
        List<WebElement> Columns =
                DatePickerWheel1.findElements(By.xpath("//XCUIElementTypePickerWheel"));
        String pattern = "yyyy-MM-dd";
        try {
            DateFormat df = new SimpleDateFormat(pattern);
            Date parsedDate = df.parse(dateSelected);

            Format monthFormat = new SimpleDateFormat("MMMM");
            String month = monthFormat.format(parsedDate);



            Format dayFormat = new SimpleDateFormat("dd");
            String day = dayFormat.format(parsedDate);



            Format yearFormat = new SimpleDateFormat("yyyy");
            String year = yearFormat.format(parsedDate);



            Columns.get(0).sendKeys(month);
            Columns.get(1).sendKeys(day);
            Columns.get(2).sendKeys(year);

            driver.findElement(By.id("Done")).click();

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }


    }


}
