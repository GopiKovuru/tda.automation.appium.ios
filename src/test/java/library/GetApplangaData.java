package library;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import drivers.Drivers;
import org.json.simple.parser.JSONParser;
import java.io.*;
import java.io.FileReader;
import java.util.HashMap;

import org.json.simple.JSONObject;

public class GetApplangaData {

    private String region;
    private String locale;

    public GetApplangaData(String region, String locale) {

        this.region = region;
        this.locale = locale;

    }

    public void get_lego_applangs_data() {


        String stringEndpoint = "";
        String authorization = "";
        String apiData = "";
        String filePath = "src/test/java/com/tui/data/jsondata" +
                "/applanga/";
        String[] localeapplangajsonfile = get_locale_applangajsonfile(region,locale);
        String requestedLanguages = localeapplangajsonfile[0];
        String jsonfileName = localeapplangajsonfile[1];

        ReusableMethods reusableMethods = new ReusableMethods();
        HashMap<String, String> environmentsHashMap = new HashMap<String,
                String>();
        switch (region) {
            case "NonFC":
                environmentsHashMap =  reusableMethods.get_environments_data
                    ("nonfcapplanga");
                stringEndpoint = environmentsHashMap.get("server_end_point");
                stringEndpoint = String.format(stringEndpoint + "?app=5666f28725641dab701ac733&1ac733&includeSrc=true&includeStatus=true&requestedLanguages=%s", requestedLanguages);
                authorization = environmentsHashMap.get("authorization");
                break;
            case "FC":
                environmentsHashMap =  reusableMethods.get_environments_data
                        ("fcapplanga");
                stringEndpoint = environmentsHashMap.get("server_end_point");
                stringEndpoint = String.format(stringEndpoint +
                        "?app=5773b384565a302f28631267&1ac733&includeSrc=true&includeStatus=true&requestedLanguages=%s",requestedLanguages);
                authorization = environmentsHashMap.get("authorization");

                break;

        }


        try {

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(stringEndpoint)
                    .get()

                    .addHeader("Authorization", authorization)

                    .build();

            Response response = client.newCall(request).execute();


            apiData = response.body().string();
            String jsonfilePath  = filePath + jsonfileName;
            ObjectMapper mapper = new ObjectMapper();

            Object jsonObject = mapper.readValue(apiData, Object.class);
            String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);

            try (FileWriter file = new FileWriter(jsonfilePath)) {

                file.write(prettyJson);
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            System.out.println("\nThis is exception");
            System.out.println(e.toString());

        }

    }

    private static String[] get_locale_applangajsonfile(String region,String
            locale)
    {
        String localeapplangajsonfile[] = new String[2];
        switch (locale) {
            case "en-GB":
                localeapplangajsonfile[0] = "[%22en%22,%22en-GB%22]";

                if( region.equalsIgnoreCase("NonFC") ) {
                    localeapplangajsonfile[1]  = "tuiapplanga.json";
                }else {
                    localeapplangajsonfile[1]  = "fcapplanga.json";
                }
                break;
            case "nl":
                localeapplangajsonfile[0] = "[%22en%22,%22nl%22]";
                localeapplangajsonfile[1] = "nlapplanga.json";
                break;
            case "fr-BE":
                localeapplangajsonfile[0] = "[%22en%22,%22fr-BE%22]";
                localeapplangajsonfile[1] = "frbeapplanga.json";
                break;
            case "nl-BE":
                localeapplangajsonfile[0] = "[%22en%22,%22nl-BE%22]";
                localeapplangajsonfile[1] = "nlbeapplanga.json";
                break;
            case "de-DE":
                localeapplangajsonfile[0] = "[%22en%22,%22de-DE%22]";
                localeapplangajsonfile[1] = "deapplanga.json";
                break;
            case "sv":
                localeapplangajsonfile[0] = "[%22en%22,%22sv%22]";
                localeapplangajsonfile[1] = "svapplanga.json";
                break;
            case "fi":
                localeapplangajsonfile[0] = "[%22en%22,%22fi%22]";
                localeapplangajsonfile[1] = "fiapplanga.json";
                break;
            case "nb":
                localeapplangajsonfile[0] = "[%22en%22,%22nb%22]";
                localeapplangajsonfile[1] = "nbapplanga.json";
                break;
            case "da":
                localeapplangajsonfile[0] = "[%22en%22,%22da%22]";
                localeapplangajsonfile[1] = "daapplanga.json";
                break;
        }

       return localeapplangajsonfile; //returning two values at once
    }

    public String read_applanga_value_from_json_file(String applangaid)
    {

        String locale = Drivers.getLocale();
        String region = Drivers.getApplangaRegion();
        String group = "main";

        if(applangaid.startsWith("ancillary")) {

           group = "Ancillary";
        }
        else if(applangaid.startsWith("bookAHoliday")) {
           group = "BookAHoliday";
        }
        else if(applangaid.startsWith("homecards")) {
           group = "Homecard";
        }
        else if(applangaid.startsWith("excursion")) {
           group = "excursion";
        }
        else if(applangaid.startsWith("gn")) {
           group = "Global Nav";
        }


        String filePath = "src/test/java/com/tui/data/jsondata" +
                "/applanga/";

        String defaultlocale = "en";

        String[] localeapplangajsonfile = get_locale_applangajsonfile(region,locale);
        String jsonfileName = localeapplangajsonfile[1];

        String jsonfilePath  = filePath + jsonfileName;
        String applanagvalue = applangaid;
        try {

            FileReader reader = new FileReader(jsonfilePath);
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
            JSONObject applangaidjson = ((JSONObject) ((JSONObject) (
                    (JSONObject)  (JSONObject) ((JSONObject) ((JSONObject) jsonObject.get("data")).get(locale)).get(group)).get("entries")).get(applangaid));

            if(applangaidjson != null)
            {
                applanagvalue  = (String) applangaidjson.get("v");

                if(applanagvalue == null) {

                    applangaidjson = ((JSONObject) ((JSONObject) (
                            (JSONObject)  (JSONObject) ((JSONObject) ((JSONObject) jsonObject.get("data")).get(defaultlocale)).get(group)).get("entries")).get(applangaid));
                    applanagvalue  = (String) applangaidjson.get("v");
                }
            }
            else
            {
                System.out.println("Can not find Applanga id " + applangaid + " " +
                        "in locale " +  locale + " for region " + region);
            }


        } catch (Exception e) {

            System.out.println(e.toString());

        }
        return applanagvalue;

    }
}
