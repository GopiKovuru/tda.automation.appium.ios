package steps;

import cucumber.api.PendingException;
import drivers.Drivers;
import library.*;
import cucumber.api.java.en.Then;
import runner.RunTest;


import java.util.HashMap;

public class RetrieveBookingSteps extends RunTest {

    @Then("^I enter valid credentials and click on submit$")
    public void iEneterValidCredentialsinRetrieveBooking() throws Throwable {


        ReusableMethods reusableMethods = new ReusableMethods();
        HashMap<String, String> testDataHashMap = reusableMethods.get_testdata(Drivers.getBrand(),"1");

        switch (Drivers.getBrand()) {
            case "tuiuk":
            case "fc":
                retrieveBookingPage.submit_uk_data(testDataHashMap.get
                                ("surname"),testDataHashMap.get("booking_reference"),
                        testDataHashMap.get("departuredate"));
                break;
            case "detui":
            case "meinetui":
                retrieveBookingPage.submit_de_data(testDataHashMap.get
                                ("username"),testDataHashMap.get("password"));
                break;
            case "betui":
            case "besj":
                retrieveBookingPage.submit_be_data(testDataHashMap.get
                        ("surname"),testDataHashMap.get("booking_reference"));
                break;
            case "nl":
                retrieveBookingPage.submit_nl_data(testDataHashMap.get
                                ("surname"),testDataHashMap.get("booking_reference"));
                break;
            case "sv":
            case "fi":
            case "nb":
            case "da":
                retrieveBookingPage.submit_nordics_data(testDataHashMap.get
                        ("booking_reference"),testDataHashMap.get("phone"));
                break;

        }

    }


}
