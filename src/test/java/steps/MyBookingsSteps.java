package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import runner.RunTest;

public class MyBookingsSteps extends RunTest {

    @Then("^I navigate to booking details$")
    public void iNavigateToBookingDetails() throws Throwable {

        myBookingPage.navigate_inside_trips();
    }
}
