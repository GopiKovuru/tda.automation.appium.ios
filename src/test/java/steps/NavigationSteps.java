package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;

import org.junit.Assert;
import pages.NavigationPage;
import runner.RunTest ;

public class NavigationSteps extends RunTest {

    @Then("^I validate Navigation Menu Items$")
    public void iValidateNavigationMenu() throws Throwable {


        String applangaid = "gn_tab1_label";
        String appalnagavalue = getApplangaData.read_applanga_value_from_json_file(applangaid);
        Assert.assertTrue(navigationPage.navigationTabVisible("Homecards",
                appalnagavalue));

        applangaid = "gn_tab2_label";
        appalnagavalue = getApplangaData.read_applanga_value_from_json_file(applangaid);
        Assert.assertTrue(navigationPage.navigationTabVisible("Search",
                appalnagavalue));

        applangaid = "gn_tab3_label";
        appalnagavalue = getApplangaData.read_applanga_value_from_json_file(applangaid);
        Assert.assertTrue(navigationPage.navigationTabVisible("Trips",
                appalnagavalue));


        applangaid = "gn_tab4_label";
        appalnagavalue = getApplangaData.read_applanga_value_from_json_file(applangaid);
        Assert.assertTrue(navigationPage.navigationTabVisible("Preferences",
                appalnagavalue));


    }

    @Then("^I click on each Navigation Menu$")
    public void iClickOnNavigationMenuItems() throws Throwable {

        navigationPage.navigateToTabs("Highlights");
        navigationPage.navigateToTabs("Search");
        navigationPage.navigateToTabs("Trips");
        navigationPage.navigateToTabs("Preferences");

    }

    @Then("^I navigate to Trips Screen$")
    public void iClickOnTripsMenuItem() throws Throwable {


        navigationPage.navigateToTabs("Trips");

    }


}
