package steps;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tui.data.models.homecardswithoutbooking.HomeCardsWithoutBookings;
import cucumber.api.java.en.Then;
import library.*;
import cucumber.api.PendingException;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import pages.HighlightsPage;
import runner.RunTest ;
import com.tui.data.models.*;
import java.util.List;

public class HighlightsSteps extends RunTest {


    @Then("^I click on each Highlights Menu Item$")
    public void iClickOnEachHighlightsMenuItem() throws Throwable {


        navigationPage.navigateToTabs("Highlights");

    }

    @Then("^I validate Highlights Screen$")
    public void iValidateHighlightsScreen() throws Throwable {
        String apiData = getLegoAPIData.get_lego_api_data
                ("homecardswithoutbooking");


        try
        {
            ObjectMapper mapper = new ObjectMapper();
            HomeCardsWithoutBookings homecards = mapper.readValue(apiData,
                    HomeCardsWithoutBookings.class);

            homecards.cards.forEach((card) -> {


                switch (card.layout.template) {
                    case "SMALL_IMAGE_TEMPLATE":
                        validate_small_image_template_homecards(card
                                .identifier,card.layout.title,card.layout
                                .subtitle);
                        break;
                    case "BIG_IMAGE_TEMPLATE":
                        validate_big_image_template_homecards(card
                                .identifier,card.layout.title,card.layout
                                .subtitle);

                        break;

                }
            });

        }
        catch(Exception e)
        {
            System.out.println("Handle this logging");
        }
    }

    private void validate_small_image_template_homecards(String identifier,
                                                         String title,
                                                         String subtitle)
    {


        highlightsPage.validate_small_image_template_homecards(identifier,
                title,subtitle);


//        System.out.println("Whats happening");
//
//        System.out.println("krishna " + highlightsPage
//                        .homecards_smallimage_title.getId());
//        System.out.println("gopi " + highlightsPage
//                .homecards_smallimage_title.getTagName());
//
//        System.out.println("hari " + appiumDriver.findElementsByName
//                ("homecards_bigimage_title").get(0).getText());
//        Assert.assertEquals(title,highlightsPage.homecards_smallimage_title
//                .getText());

        ;

    }

    private void validate_big_image_template_homecards(String identifier,
                                                         String title,
                                                         String subtitle)
    {
//        Assert.assertEquals(highlightsPage.homecards_bigimage_title.getText()
//                , title);

    }
}
