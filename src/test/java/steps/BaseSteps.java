package steps;

import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;

import java.net.URL;
import runner.RunTest ;

public class BaseSteps extends RunTest {
//    protected  AppiumDriver appiumDriver;

    @When("^TUI App is Launched and I allow notifications$")
    public void iLaunchIOSApp() throws Throwable {

      if(  appiumDriver.findElements(By.id("Allow")).size() > 0 )
      {
          appiumDriver.findElement(By.id("Allow")).click();
      }

    }
}
