
package com.tui.data.models.homecardswithoutbooking;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "portrait",
    "landscape"
})
public class Image {

    @JsonProperty("portrait")
    public String portrait;
    @JsonProperty("landscape")
    public String landscape;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("portrait", portrait).append("landscape", landscape).toString();
    }

}
