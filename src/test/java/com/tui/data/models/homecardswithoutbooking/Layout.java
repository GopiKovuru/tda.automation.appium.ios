
package com.tui.data.models.homecardswithoutbooking;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "title",
    "subtitle",
    "icon",
    "template",
    "type",
    "image"
})
public class Layout {

    @JsonProperty("title")
    public String title;
    @JsonProperty("subtitle")
    public String subtitle;
    @JsonProperty("icon")
    public Object icon;
    @JsonProperty("template")
    public String template;
    @JsonProperty("type")
    public String type;
    @JsonProperty("image")
    public Image image;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("title", title).append("subtitle", subtitle).append("icon", icon).append("template", template).append("type", type).append("image", image).toString();
    }

}
