
package com.tui.data.models.homecardswithoutbooking;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "identifier",
    "layout",
    "action",
    "priority",
    "type"
})
public class Card {

    @JsonProperty("identifier")
    public String identifier;
    @JsonProperty("layout")
    public Layout layout;
    @JsonProperty("action")
    public Action action;
    @JsonProperty("priority")
    public long priority;
    @JsonProperty("type")
    public String type;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("identifier", identifier).append("layout", layout).append("action", action).append("priority", priority).append("type", type).toString();
    }

}
