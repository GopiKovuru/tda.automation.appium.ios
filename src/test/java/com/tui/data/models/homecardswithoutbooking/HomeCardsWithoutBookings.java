
package com.tui.data.models.homecardswithoutbooking;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cards"
})
public class HomeCardsWithoutBookings {

    @JsonProperty("cards")
    public List<Card> cards = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cards", cards).toString();
    }

}
