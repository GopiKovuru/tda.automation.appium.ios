package runner;

import drivers.Drivers;
import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.junit.Cucumber;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import library.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.DesiredCapabilities;
import pages.*;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features",
        glue= {"steps"},
        plugin = { "pretty", "json:target/cucumber-reports/Cucumber.json",
                "junit:target/cucumber-reports/Cucumber.xml",
                "html:target/cucumber-reports"},
        monochrome = true
)
public class RunTest {

    private static AppiumDriverLocalService appiumService;
    public static AppiumDriver<MobileElement> appiumDriver;
    public static NavigationPage navigationPage;
    public static HighlightsPage highlightsPage;
    public static RetrieveBookingPage retrieveBookingPage;
    public static MyBookingsPage myBookingPage;
    public static GetApplangaData getApplangaData;
    public static GetLegoAPIData getLegoAPIData;



    @BeforeClass
    public static void setUp() throws IOException {


        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();

        String environment = Drivers.getEnvironment();
        String brand = Drivers.getBrand();
        String locale = Drivers.getLocale();
        appiumDriver = Drivers.getDriver();


        switch (brand) {
            case "tuiuk":
            case "detui":
            case "meinetui":
            case "betui":
            case "besj":
            case "nl":
            case "sv":
            case "fi":
            case "nb":
            case "da":
                getApplangaData = new GetApplangaData("NonFC",locale);
                break;
            case "fc":
                getApplangaData = new GetApplangaData("FC",locale);
                break;
        }

        try {


            getLegoAPIData = new GetLegoAPIData(environment,brand);
            getApplangaData.get_lego_applangs_data();
            navigationPage = new NavigationPage(appiumDriver);
            highlightsPage = new HighlightsPage(appiumDriver);

            retrieveBookingPage = new RetrieveBookingPage(appiumDriver);
            myBookingPage = new MyBookingsPage(appiumDriver);

        } catch (Exception e) {

            System.out.println("Exception while running setup :" + e.getMessage
                    ());
        }

    }

    @AfterClass
    public static void tearDown() {
        try {

            appiumDriver.quit();
            appiumService.stop();
        } catch (Exception e) {
            System.out.println("Exception while running Tear down :" + e.getMessage());
        }
    }
}
