package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.PageFactory;

public class MyBookingsPage  extends BasePage {

    AppiumDriver<MobileElement> appiumDriver;

    @iOSFindBy(accessibility = "account_list_item_row_0_0")
    private MobileElement account_list_item_row_0_0;



    public MyBookingsPage(AppiumDriver<MobileElement> appiumDriver) throws Exception {
        super(appiumDriver);
        this.appiumDriver = appiumDriver;
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver), this);
    }

    public void navigate_inside_trips()
    {

        account_list_item_row_0_0.click();

        try
        {
            Thread.sleep(10000);
        }
        catch(Exception e) {
            System.out.println("Crashed");
        }
    }
}
