package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

public class HighlightsPage  extends BasePage {

    AppiumDriver<MobileElement> appiumDriver;

    @iOSFindBy(accessibility = "homecards_bigimage_title")
    public MobileElement homecards_bigimage_title;

    @iOSFindBy(accessibility = "homecards_bigimage_subtitle")
    public MobileElement homecards_bigimage_subtitle;

    @iOSFindBy(accessibility = "homecards_smallimage_title")
    public MobileElement homecards_smallimage_title;

    @iOSFindBy(accessibility = "homecards_smallimage_subtitle")
    public MobileElement homecards_smallimage_subtitle;




    public HighlightsPage(AppiumDriver<MobileElement> appiumDriver) throws Exception {
        super(appiumDriver);
        this.appiumDriver = appiumDriver;
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver), this);
    }

    public void validate_small_image_template_homecards(String identifier,
                                               String title,
                                               String subtitle)
    {

//        MobileElement elTitle = appiumDriver.
//        Assert.assertTrue(elTitle.isDisplayed());
//        MobileElement elSubTitle = appiumDriver.findElementByName(subtitle);
//        Assert.assertTrue(elSubTitle.isDisplayed());
    }

    public void validate_big_image_template_homecards(String identifier,
                                                        String title,
                                                        String subtitle)
    {



    }
}
