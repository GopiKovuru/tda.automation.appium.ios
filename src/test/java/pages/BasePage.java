package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BasePage {

    private AppiumDriver<MobileElement> driver;

//    private WebDriverWait wait;

    public BasePage(AppiumDriver<MobileElement> driver) throws Exception {
        this.driver = driver;

    }

//    public void waitForElementToBeVisible(AppiumDriver<MobileElement> element) {
//        wait.until(ExpectedConditions.visibilityOf(element));
//    }
}
