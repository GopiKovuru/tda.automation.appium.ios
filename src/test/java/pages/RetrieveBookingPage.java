package pages;

import io.appium.java_client.AppiumDriver;
import  io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import library.*;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

public class RetrieveBookingPage  extends BasePage {

    AppiumDriver<MobileElement> appiumDriver;

    @iOSFindBy(accessibility = "add_booking_input_content_surname")
    private MobileElement add_booking_input_content_surname;

    @iOSFindBy(accessibility = "add_booking_input_content_departure_date")
    private MobileElement add_booking_input_content_departure_date;

    @iOSFindBy(accessibility = "add_booking_input_content_booking_code")
    private MobileElement add_booking_input_content_booking_code;

    @iOSFindBy(accessibility = "add_booking_btn_add_holiday")
    private MobileElement add_booking_btn_add_holiday;

    @iOSFindBy(accessibility = "user_login_username_item")
    private MobileElement user_login_username_item;

    @iOSFindBy(accessibility = "user_login_password_item")
    private MobileElement user_login_password_item;

    @iOSFindBy(accessibility = "add_booking_input_content_tel_email")
    private MobileElement add_booking_input_content_tel_email;



    public RetrieveBookingPage(AppiumDriver<MobileElement> appiumDriver) throws Exception {
        super(appiumDriver);
        this.appiumDriver = appiumDriver;
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver), this);
    }

    public void submit_uk_data(String surname,String bookingReference,
                                  String bookingDate)
    {
        add_booking_input_content_surname.setValue(surname);
//        hidekeyboard();

        add_booking_input_content_departure_date.click();

        IOSDriver iosDriver = (IOSDriver)appiumDriver;

        ReusableUIMethods reusableuiMethods = new
                ReusableUIMethods();

        reusableuiMethods.pick_ios_date (iosDriver, bookingDate);

        add_booking_input_content_booking_code.setValue(bookingReference);

        //        hidekeyboard();

        add_booking_btn_add_holiday.click();
        try
        {
            Thread.sleep(10000);
        }
        catch(Exception e) {
            System.out.println("Crashed");
        }

    }

    public void submit_nl_data(String surname,String bookingReference)
    {
        add_booking_input_content_surname.setValue(surname);
        hidekeyboard();
        add_booking_input_content_booking_code.setValue(bookingReference);
        hidekeyboard();
        add_booking_btn_add_holiday.click();

        try
        {
            Thread.sleep(10000);
        }
        catch(Exception e) {
            System.out.println("Crashed");
        }
    }

    public void submit_de_data(String username,String password)
    {
        user_login_username_item.setValue(username);
        hidekeyboard();
        user_login_password_item.setValue(password);
        hidekeyboard();
        try
        {
            Thread.sleep(10000);
        }
        catch(Exception e) {

            System.out.println("Crashed");
        }

    }

    public void submit_nordics_data(String bookingReference,String phoneOrEmail)
    {
        add_booking_input_content_booking_code.setValue(bookingReference);

        hidekeyboard();
        add_booking_input_content_tel_email.setValue(phoneOrEmail);

        hidekeyboard();
        add_booking_btn_add_holiday.click();

        try
        {
            Thread.sleep(10000);
        }
        catch(Exception e) {
            System.out.println("Crashed");
        }
    }

    public void submit_be_data(String surname,String bookingReference)
    {
        add_booking_input_content_surname.setValue(surname);
        hidekeyboard();
        add_booking_input_content_booking_code.setValue(bookingReference);
        hidekeyboard();
        add_booking_btn_add_holiday.click();

        try
        {
            Thread.sleep(10000);

        }
        catch(Exception e) {
            System.out.println("Crashed");
        }
    }

    private void hidekeyboard()
    {
        appiumDriver.hideKeyboard();
    }
}
