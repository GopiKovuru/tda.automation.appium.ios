package pages;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.support.FindBy;

public class NavigationPage  extends BasePage {

    AppiumDriver<MobileElement> appiumDriver;

    @iOSFindBy(accessibility = "home_menu_item_home")
    public MobileElement home_menu_item_home;

    @iOSFindBy(accessibility = "home_menu_item_book_a_holiday")
    public MobileElement home_menu_item_book_a_holiday;

    @iOSFindBy(accessibility = "home_menu_item_authentication")
    public MobileElement home_menu_item_authentication;

    @iOSFindBy(accessibility = "home_menu_item_preferences")
    public MobileElement home_menu_item_preferences;

    public NavigationPage(AppiumDriver<MobileElement> appiumDriver) throws Exception {
        super(appiumDriver);
        this.appiumDriver = appiumDriver;
        PageFactory.initElements(new AppiumFieldDecorator(appiumDriver), this);
    }


    public void navigateToTabs(String tabName) {

        switch (tabName) {
            case "Highlights":
                home_menu_item_home.click();
                break;
            case "Search":
                home_menu_item_book_a_holiday.click();
                break;
            case "Trips":
                home_menu_item_authentication.click();
                break;
            case "Preferences":
                home_menu_item_preferences.click();
                break;
        }
    }
    public boolean navigationTabVisible(String tabName, String applangaValue)

    {
        boolean isNavigationTabVisble = false;
        switch (tabName) {
            case "Homecards":
                isNavigationTabVisble = home_menu_item_home.isDisplayed();
                break;
            case "Search":
                isNavigationTabVisble = home_menu_item_book_a_holiday.isDisplayed();
                break;
            case "Trips":
                isNavigationTabVisble = home_menu_item_authentication.isDisplayed();
                break;
            case "Preferences":
                isNavigationTabVisble = home_menu_item_preferences.isDisplayed();
                break;

        }
        return isNavigationTabVisble;

    }

}
