package drivers;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public class Drivers {

    private static DesiredCapabilities desiredCapabilities = null;
    private static String brand = null;
    private static String devicetype = null;
    private static String env = null;
    private static String locale = null;
    private static String country = null;
    private static String applangaregion = null;
    private static String testData = null;

    public static AppiumDriver<MobileElement> appiumDriver;
    Properties prop = new Properties();

    public static String getDeviceType() {
        devicetype = System.getProperty("devicetype");
        return devicetype;
    }

    public static String getBrand() {
        brand = System.getProperty("brand");
        return brand;
    }

    public static String getEnvironment() {
        env = System.getProperty("env");
        return env;
    }


    public static String getCountry() {
        env = System.getProperty("country");
        return country;
    }

    public static String getTestData() {
        testData = System.getProperty("testData");
        return testData;
    }



    public static String getLocale() {
        locale = "";

        switch (getBrand()) {
            case "tuiuk":
                locale = "en-GB";
                break;
            case "detui":
                locale = "de-DE";
                break;
            case "meinetui":
                locale = "de-DE";
                break;
            case "betui":
                locale = System.getProperty("locale");
                break;
            case "besj":
                locale = System.getProperty("locale");
                break;
            case "nl":
                locale = "nl";
                break;
            case "sv":
                locale = "sv";
                break;
            case "fi":
                locale = "fi";
                break;
            case "nb":
                locale = "nb";
                break;
            case "da":
                locale = "da";
                break;
            case "fc":
                locale = "en-GB";
                break;

        }

        return locale;
    }

    public static String getApplangaRegion() {
        applangaregion = "";
        switch (getBrand()) {
            case "tuiuk":
            case "detui":
            case "meinetui":
            case "betui":
            case "besj":
            case "nl":
            case "sv":
            case "fi":
            case "nb":
            case "da":
                applangaregion = "NonFC";
                break;
            case "fc":
                applangaregion = "FC";
                break;

        }
        return applangaregion;
    }

    public static AppiumDriver<MobileElement> getDriver() {

       desiredCapabilities = new DesiredCapabilities();

       String appName = "" ;

        switch (getBrand()) {
            case "tuiuk":
                appName = "UK-TH.app";
                break;
            case "fc":
                appName = "UK-FC.app";
                break;
            case "detui":
                appName = "DE-TC.app";
                break;
            case "meinetui":
                appName = "DE-MT.app";
                break;
            case "betui":
                appName = "BE-JA.app";
                break;
            case "besj":
                appName = "BE-SJ.app";
                break;
            case "nl":
                appName = "NL.app";
                break;
            case "sv":
                appName = "NO-SV.app";
                break;
            case "fi":
                appName = "NO-FI.app";
                break;
            case "nb":
                appName = "NO-NO.app";
                break;
            case "da":
                appName = "NO-DK.app";
                break;

        }
        try
        {

            desiredCapabilities.setCapability("appium-version", "1.8.1");
            desiredCapabilities.setCapability("automationName", "XCUITest");
            desiredCapabilities.setCapability("platformName", "iOS");
            desiredCapabilities.setCapability("platformVersion", "11.4");
            desiredCapabilities.setCapability("deviceName", "iPhone 6");


            desiredCapabilities.setCapability("bootstrapPath", "/usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent");
            desiredCapabilities.setCapability("agentPath", "/usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent/WebDriverAgent.xcodeproj");

            env = getEnvironment();
            desiredCapabilities.setCapability("app", "src/app/" + env + "/" +
                    appName);
//            desiredCapabilities.setCapability("noReset", false);
//            desiredCapabilities.setCapability("fullReset", true);
//            desiredCapabilities.setCapability(MobileCapabilityType
//                    .FULL_RESET,true);
            desiredCapabilities.setCapability("autoAcceptAlerts", true);

            appiumDriver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), desiredCapabilities);

            appiumDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            return appiumDriver;
        }
        catch (Exception e) {
            System.out.println("Exception while running Tear down :" + e.getMessage());
        }
        return appiumDriver;
    }


}
