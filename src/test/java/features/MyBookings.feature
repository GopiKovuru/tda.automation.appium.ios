Feature: Validates the My Bookings Screen

  @iosnavigatetobookingdetails @ioscompleteregression @tc1234
  Scenario: Validates Retrieve a Booking Screen


    Given TUI App is Launched and I allow notifications
    Then I navigate to Trips Screen
    Then I enter valid credentials and click on submit
    Then I navigate to booking details